print("Máš dlouhé jméno?")
jmeno = input("Jak se jmenuješ?")
dlouhe = len(jmeno) > 7
kratke = len(jmeno) <= 3
if dlouhe:
    print("Teda to je dlouhé jméno!")
elif kratke:
    print("Tak krátké jméno se dobře píše")
else:
    print("Ani krátké, ani dlouhé. Tak akorát.")
