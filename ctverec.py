# Tento program počítá pičoviny okolo čtverce
strana = float(input("Zadej číslo: ")) # Tady zadáš číslo v centimetrech
print("Obvod čtverce se stranou", strana, "cm je", 4 * strana, "cm")
print("Obsah čtverce se stranou", strana, "cm je", strana * strana, "cm")