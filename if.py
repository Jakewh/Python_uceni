strana = float(input('Zadej stranu čtverce v centimetrech: '))
true = strana > 0

if true:
    print('Obvod čtverce se stranou', strana, 'je', 4 * strana, 'cm')
    print('Obsah čtverce se stranou', strana, 'je', strana * strana, 'cm2')
else:
    print("Číslo musí být kladné!")