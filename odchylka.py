#!/usr/bin/env python3

"""
    _____ _______         _                      _
   |_   _|__   __|       | |                    | |
     | |    | |_ __   ___| |___      _____  _ __| | __  ___ ____
     | |    | | '_ \ / _ \ __\ \ /\ / / _ \| '__| |/ / / __|_  /
    _| |_   | | | | |  __/ |_ \ V  V / (_) | |  |   < | (__ / /
   |_____|  |_|_| |_|\___|\__| \_/\_/ \___/|_|  |_|\_(_)___/___|
   
   IT ZPRAVODAJSTVÍ  <>  PROGRAMOVÁNÍ  <>  HW A SW  <>  KOMUNITA
   
   Tento zdrojový kód je součástí výukových seriálů na 
   IT sociální síti WWW.ITNETWORK.CZ

   Kód spadá pod licenci prémiového obsahu a vznikl díky podpoře
   našich členů. Je určen pouze pro osobní užití a nesmí být šířen.
"""
cisla = []
vstup = "vstup" # nesmí být vyhodnoceno jako False, aby se spustil cyklus
print("Zadávej čísla a nakonec zadej pouze ENTER pro ukončení zadávání")
while vstup:
    vstup = input("Zadej číslo: ")
    if vstup != "":
        cisla.append(int(vstup))
cisla_2 = sorted(cisla)
# zjednodušený medián 
median = cisla_2[len(cisla_2)//2]
for cislo in cisla:
    print(cislo, "se od mediánu odlišuje o", cislo - median)
input()

    
