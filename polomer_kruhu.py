print("Pomůžu ti spočítat obvod a obsah kruhu")
polomer = float(input("Zadej poloměr kruhu v centimetrech: "))
print("Obvod kruhu s poloměrem", polomer, "cm je", 2 * 3.14 * polomer, "cm")
print("Obsah kruhu s polomerem", polomer, "cm je", 3.14 * polomer ** 2, "cm")